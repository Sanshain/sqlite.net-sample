﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SQLite.Net;
using System.Diagnostics;

namespace WindowsFormsApplication1
{


    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Process.Start(Application.StartupPath);

            List<Contacts> cnts = new List<Contacts>();
            for (var i = 0; i < 50; i++)
                cnts.Add(new Contacts { Phone = i.ToString(), Name = DateTime.Now.TimeOfDay.ToString() });

            Stopwatch sw = new Stopwatch();
            sw.Start();

            using (var conn = new SQLiteConnection("base.db"))
            {
                conn.DropTable<Contacts>();

                var fl = conn.CreateTable<Contacts>();

                //conn.InsertAll(cnts);

                int c = 0;
                conn.RunInTransaction(() => {
                    foreach (var cnt in cnts)
                    {
                        c += conn.InsertOrReplace(cnt);
                    }
                });//*/


                // conn.UpdateAll(cnts);

                /*
                int c = 0;
                foreach (var cnt in cnts)
                {
                    c += conn.InsertOrReplace(cnt);
                }//*/
                
            }

            sw.Stop();
            this.Text = sw.ElapsedMilliseconds.ToString();

            (sender as Button).Text = "ok";

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            using (var conn = new SQLiteConnection("base.db"))
            {
                var fl = conn.Table<Contacts>().ToList();

                listBox1.DataSource = fl;
            }
        }

        private void UpdateBtn_Click(object sender, EventArgs e)
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();

            using (var conn = new SQLiteConnection("base.db"))
            {
                var fl = conn.Table<Contacts>().ToList();

                listBox1.DataSource = fl;

                (sender as Button).Text = fl.Count.ToString();
            }

            sw.Stop();
            this.Text = sw.ElapsedMilliseconds.ToString();

        }
    }

    public class Contacts
    {


        [PrimaryKey, Column("_Number")]
        public string Phone { get; set; }
        public string Name { get; set; }
        public string Photo { get; set; }
        public string OptionalPhones { get; set; }


        public override string ToString()
        {
            return this.Phone + " (" + this.Name + ")";
        }
    }    


}
